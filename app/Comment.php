<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = ['event_id', 'user_id', 'comment', 'type'];
    public $timestamps = true;

    public function event()
    {
        return $this->belongsTo('App\Activity', 'event_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
