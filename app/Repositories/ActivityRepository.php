<?php

namespace App\Repositories;

use App\Comment;
use App\Activity;

class ActivityRepository
{
    protected $model;

    public function __construct(Activity $activities)
    {
        $this->model = $activities;
    }

    /**
     * Get all Activities
     *
     * @return mixed
     */
    public function getFeed($limit)
    {
        $query = $this->model->with('comments.user', 'latestCommentUsers.user')
            ->with(['user.photos' => function ($query) {
                $query->selectRaw("*, CONCAT(
                        GROUP_CONCAT(id,':',server,':',photo ORDER BY id DESC SEPARATOR '~')
                    ) AS phts")->groupBy('id');
            }])
            ->orderBy('created_at', 'desc')
            ->paginate($limit);

        return $query;
    }

    /**
     * Get all Activities Comments by PostId
     *
     * @return mixed
     */
    public function getFeedComments($postId, $limit)
    {
        $commentModel = new Comment();

        $query = $commentModel->with('user')
            ->where('event_id', $postId)
            ->orderBy('created_at', 'asc')
            ->paginate($limit);

        return $query;
    }
}
