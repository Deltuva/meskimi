<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifies';

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
