<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLike extends Model
{
    protected $table = 'eventlikes';
    protected $fillable = ['user_id', 'event_id'];

}
