<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use App\Http\Requests\ActivityEventCommentCreateRequest;

class ActivityCommentsCreateController extends Controller
{
    public function storeNewComment(ActivityEventCommentCreateRequest $request, $postId)
    {
        if ($request->ajax()) {
            try {
                $comment = new Comment;
                $isCommented = $comment->create([
                    'event_id' => $postId,
                    'user_id' => auth()->user()->id,
                    'comment' => $request->comment,
                    'type' => 'post'
                ]);

                if ($isCommented) {
                    return response()
                    ->json([
                        'status' => 'success'
                    ], 200);
                }
            } catch (QueryException $e) {
                return back()
                ->withError('StoreNewComment Error: ' . $e->getMessage())
                ->withInput();
            }
        }
    }
}
