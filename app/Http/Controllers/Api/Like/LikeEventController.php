<?php

namespace App\Http\Controllers\Api\Like;

use App\ActivityLike;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;

class LikeEventController extends Controller
{
    public function countLikes($postId)
    {
        $like = new ActivityLike;
        $likes = $like->whereEventId($postId)->get();
        return response()
                ->json($likes, 200);
    }

    public function handleLike($postId)
    {
        try {
            $like = new ActivityLike;
            $authUserId = auth()->user()->id;
            $existingLike = $like
                ->whereEventId($postId)->whereUserId($authUserId)->first();

            if (is_null($existingLike)) {
                $isLiked = $like->create([
                    'event_id' => $postId,
                    'user_id' => $authUserId
                ]);
                if ($isLiked) {
                    return response()
                    ->json([
                        'status' => true
                    ], 200);
                }
            }
        } catch (QueryException $e) {
            return back()
                ->withError('LikeEventLiked Error: ' . $e->getMessage())
                ->withInput();
        }
    }

    public function checkLikeUserExisted($postId)
    {
        $like = new ActivityLike;
        $authUserId = auth()->user()->id;
        $isUserExisted = $like->whereEventId($postId)->whereUserId($authUserId)->count();

        if ($isUserExisted > 0) {
            return response()
                ->json([
                    'status' => true
                ], 200);
        }
    }
}
