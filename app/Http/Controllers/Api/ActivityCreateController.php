<?php

namespace App\Http\Controllers\Api;

use App\Activity;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use App\Http\Requests\ActivityEventCreateRequest;

class ActivityCreateController extends Controller
{
    public function storeNewEvent(ActivityEventCreateRequest $request)
    {
        if ($request->ajax()) {
            try {
                // create new event feed
                $event = new Activity;
                $isCreated = $event->create([
                    'user_id'     => auth()->user()->id,
                    'stream_type' => 'user',
                    'etype'       => 1,
                    'source_id'   => 0,
                    'comment'     => $request->message
                ]);

                if ($isCreated) {
                    // event json
                    return response()
                    ->json([
                        'status' => 'success'
                    ], 200);
                }
            } catch (QueryException $e) {
                return back()
                ->withError('StoreNewEvent Error: ' . $e->getMessage())
                ->withInput();
            }
        }
    }
}
