<?php

namespace App\Http\Controllers\Api;

use App\Repositories\ActivityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
    protected $activityRepo;

    public function __construct(
        ActivityRepository $activityRepo
    ) {
        $this->activityRepo = $activityRepo;
    }

    public function getData(Request $request)
    {
        $activities = $this->activityRepo->getFeed((int) $request->get('limit', 10));

        return response()
            ->json($activities);
    }
}
