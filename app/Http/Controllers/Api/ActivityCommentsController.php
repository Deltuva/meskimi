<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ActivityRepository;

class ActivityCommentsController extends Controller
{
    protected $activityRepo;

    public function __construct(
        ActivityRepository $activityRepo
    ) {
        $this->activityRepo = $activityRepo;
    }

    public function getData($postId, Request $request)
    {
        $activities = $this->activityRepo->getFeedComments($postId, (int) $request->get('limit', 10));

        return response()
            ->json($activities);
    }
}
