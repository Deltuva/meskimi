<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Activity $activityModel)
    {
        try {
            //user activity feed
            $activities = $activityModel::select('*')->get();

            //dd($activities);
        } catch (\Throwable $th) {
            //throw $th;
        }

        // $streams = DB::table('streams')
        //     ->select(DB::raw("SELECT *, SUM(actions_in_user_group) AS total_rows_in_group,
        //             GROUP_CONCAT(actions_in_user_collection) AS complete_collection
        //             FROM
        //                 ( SELECT stream.*, follows.following_user_id AS fuser,
        //                     COUNT(stream.id) AS actions_in_user_group,
        //                     GROUP_CONCAT(stream.id) AS actions_in_user_collection
        //                 FROM streams AS stream
        //                 INNER JOIN follows
        //                 ON stream.user_id = follows.following_user_id
        //                 WHERE follows.user_id = '1'
        //                     AND stream.hidden = '0'
        //                 GROUP BY stream.user_id,
        //                         stream.created_at
        //                 ) AS gr
        //             GROUP BY gr.traf_id,
        //                     gr.traf_type
        //             ORDER BY gr.created_at DESC"))
        //     ->get();

        // $data = DB::table("streams")
        //     ->select("*", "(SUM(actions_in_user_group) AS total_rows_in_group,
        //             GROUP_CONCAT(actions_in_user_collection) AS complete_collection)")
        //     ->join(DB::raw("(SELECT
        //         product_stock.id_product,
        //         GROUP_CONCAT(product_stock.quantity) as quantity_group
        //         FROM product_stock
        //         GROUP BY product_stock.id_product
        //         ) as product_stock"), function ($join) {
        //                     $join->on("product_stock.id_product", "=", "products.id");
        //                 })
        //     ->groupBy("products.id")
        //     ->get();

        //print_r($data);

        // $survey = DB::table('streams')
        //     ->join('question_survey', function ($join) {
        //         $join->on('questions.id', '=', 'question_survey.question_id')
        //             ->where('question_survey.survey_id', '=', 1);
        //     })
        //     ->leftJoin('answer_question_survey', 'question_survey.id', '=', 'answer_question_survey.question_survey_id')
        //     ->leftJoin('answers', 'answer_question_survey.answer_id', '=', 'answers.id')
        //     ->leftJoin('input-types', 'input-types.id', '=', 'questions.input-type_id')
        //     ->leftJoin(DB::raw(...), 'input-types.input-group.id', '=', '?.groups.id')
        //     ->select(
        //         'questions.id AS question.id',
        //         'questions.text AS question.text',
        //         'questions.explanation AS question.explanation',
        //         'questions.is_required AS question.is_required',
        //         'questions.input-type_id AS question.input-type_id',
        //         'input-types.id AS input-type.id',
        //         'input-types.name AS input-type.name',
        //         'input-types.input-group_id AS input-type.input-group_id',
        //         'question_survey.id AS question_survey.id',
        //         'question_survey.question_id AS question_survey.question_id',
        //         'question_survey.survey_id AS question_survey.survey_id',
        //         'question_survey.parent_id AS question_survey.parent_id',
        //         'answer_question_survey.id AS answer_question_survey.id',
        //         'answer_question_survey.question_survey_id AS answer_question_survey.question_survey_id',
        //         'answer_question_survey.answer_id AS answer_question_survey.answer_id',
        //         'answers.id AS answer.id',
        //         'answers.text AS answer.text'
        //     )
        //     ->get();

        return view('home');
    }
}
