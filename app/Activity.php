<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'events';
    protected $fillable = ['user_id', 'stream_type', 'etype', 'source_id', 'comment', 'choosed'];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'event_id', 'id');
    }

    public function latestCommentUsers()
    {
        return $this->comments()->latest();
    }
}
