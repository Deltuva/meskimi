<?php
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Private Messages routes
Route::view('messages', 'inbox.index');

Route::prefix('api')->group(function () {
    // Event feed routes
    Route::get('feed', 'Api\ActivityController@getData')->name('api.feed');
    Route::post('feed/create', 'Api\ActivityCreateController@storeNewEvent')->name('api.feed.create');
    Route::get('feed/comments/{postId}', 'Api\ActivityCommentsController@getData')->name('api.feed.comments');
    Route::post('feed/comments/{postId}/create', 'Api\ActivityCommentsCreateController@storeNewComment')->name('api.feed.comments.create');
    // Likes feed routes
    Route::get('feed/likes/{postId}/count', 'Api\Like\LikeEventController@countLikes')->name('api.feed.likes.count');
    Route::post('feed/likes/{postId}/like', 'Api\Like\LikeEventController@handleLike')->name('api.feed.likes.like');
    Route::get('feed/likes/{postId}/checkLikedByUser', 'Api\Like\LikeEventController@checkLikeUserExisted')->name('api.feed.likes.liked');
});
