<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Photo;
use Faker\Generator as Faker;

$factory->define(Photo::class, function (Faker $faker) {
    return [
        'user_id' => mt_rand(1, 2),
        'type' => $faker->randomElement(['u', 'b']),
        'event' => $faker->randomElement([3]),
        'server' => $faker->randomElement([1, 2]),
        'photo' => $faker->randomElement(['http://localhost:8000/photos/maz.png', 'http://localhost:8000/photos/cik.jpg']),
        'comment' => $faker->sentence($nbWords = 6, $variableNbWords = true)
    ];
});
