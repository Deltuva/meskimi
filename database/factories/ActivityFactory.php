<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Activity;
use Faker\Generator as Faker;

$factory->define(Activity::class, function (Faker $faker) {
    return [
        'user_id' => mt_rand(1, 2),
        'stream_type' => $faker->randomElement(['user']),
        'etype' => $faker->randomElement([3]),
        'source_id' => mt_rand(1, 2),
        'comment' => $faker->sentence($nbWords = 6, $variableNbWords = true)
    ];
});
