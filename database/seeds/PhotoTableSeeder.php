<?php

use App\Photo;
use Illuminate\Database\Seeder;

class PhotoTableSeeder extends Seeder
{
    const PHOTO_LIMIT = 1;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Photo::class, self::PHOTO_LIMIT)->create();
    }
}
