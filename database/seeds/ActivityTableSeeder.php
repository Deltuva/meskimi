<?php

use App\Activity;
use Illuminate\Database\Seeder;

class ActivityTableSeeder extends Seeder
{
    const ACTIVITY_LIMIT = 1;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Activity::class, self::ACTIVITY_LIMIT)->create();
    }
}
