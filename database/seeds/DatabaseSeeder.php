<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->command->info("Users table seeded.");

        $this->call(ActivityTableSeeder::class);
        $this->command->info("Activities table seeded.");

        $this->call(PhotoTableSeeder::class);
        $this->command->info("Photos table seeded.");
    }
}
