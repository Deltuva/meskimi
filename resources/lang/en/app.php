<?php

return [
    'navbar' => [
        'home'     => 'Home',
        'events'   => 'Events',
        'views'    => 'Views',
        'messages' => 'Messages',
    ]
];
